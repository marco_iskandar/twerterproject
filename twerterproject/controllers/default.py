# -*- coding: utf-8 -*-
# this file is released under public domain and you can use without limitations

###########################################################################
## This is central controller for Twerter, a Twitter clone, or an attempt##
## at one.                                                               ##
## Some of the code snippets from search and follow, in particular the   ##
## reduce(lambda a,b:a&b,....) was used from user neilj off stackoverflow##
## The main functions are all commented on to help explain and show the  ##
## use of particular sections of code.                                   ##
## In addition we used parts of a template for the visuals, ie the css.  ##
###########################################################################
from gluon.tools import Crud
crud = Crud(db)

#Search for friends
@auth.requires_login()
def search():
    form = SQLFORM.factory(Field('name', requires=IS_NOT_EMPTY(), label="Search for Friends" ))
    if form.accepts(request):
        check = form.vars.name.split()
        #Lambda is essentially a temp function, and here we use reduce and return a value in search for any iteration of a name
        #received by the form submission's body text. This is used several times throughout our program for search returns.
        search = reduce(lambda a,b:a&b,
                          [db.auth_user.first_name.contains(c)|db.auth_user.last_name.contains(c) \
                            for c in check])
        #Once we have a list of all users that have letters/words in common with the query, we return those that match the query and are users that are followees.
        bodies = db(search).select(orderby=db.auth_user.first_name|db.auth_user.last_name,left=db.follows.on(db.follows.followees == db.auth_user.id))
        #Attempt to sort the users into different lists but as of right now this serves little purpose.
        templistAUTH = []
        templistUSER = []
        for user in bodies:
            if user.auth_user.id == auth.user_id:
                temp = user
                break
            else:
                temp = user
    else:
        bodies = []
    return locals()

#Returns all the users you follow, needs a fix eventually
def following():
    #This was a easy way to return all of those we follow, it's not entirely efficient as it can be very slow in large requests,
    #but works for the time being.
    #We first make a table/list of vowels, and the search used below using lambda will search all users that have vowels in their names,
    #this of course returns everyone... so we need to do more
    query = ["a", "e", "i", "o", "u", "y"]
    checkDB = reduce(lambda a,b:a&b,
                          [db.auth_user.first_name.contains(query)|db.auth_user.last_name.contains(query) \
                            for q in query])
    #After we have a list of all users, we look through all the users, and use a left outer join to sort those of the results actually return
    #as a followee. Again this is incredibly inefficient for time and would need to be changed once we have a larger DB.
    following = db(checkDB).select(orderby=db.auth_user.first_name|db.auth_user.last_name,left=db.follows.on(db.follows.followees==db.auth_user.id))
    return locals()

#Returns all the people that follow you, but still buggy, it the user as a follower.
def followers():
    #This was a easy way to return all of those that follow you, it's not entirely efficient as it can be very slow in large requests,
    #but works for the time being.
    #We first make a table/list of vowels, and the search used below using lambda will search all users that have vowels in their names,
    #this of course returns everyone... so we need to do more here.
    query = ["a", "e", "i", "o", "u", "y"]
    checkDB = reduce(lambda a,b:a&b,
                          [db.auth_user.first_name.contains(query)|db.auth_user.last_name.contains(query) \
                            for q in query])
    #After we have a list of all users, we look through all the users, and use a left outer join to sort those of the results actually return
    #as a follower. Again this is incredibly inefficient for time and would need to be changed once we have a larger DB.
    followers = db(checkDB).select(orderby=db.auth_user.first_name|db.auth_user.last_name,left=db.follows.on(db.follows.followers==db.auth_user.id))
    return locals()

#Show all of the tweets from those you follow and your own tweets
@auth.requires_login()
def tweets():
    #Form for listing the tweets out.
    db.tweeters.author.default = auth.user_id
    db.tweeters.date_posted.default = request.now
    crud.settings.formstyle = 'table2cols'
    form = crud.create(db.tweeters)
    #We need to get a value for those we follow and return the value, so we check if those in DB follows are following us.
    _followees = db(db.follows.followers == auth.user_id)
    #This combines the values of those we follow (their ids) and the users ID so we have a combined list of all of our followers id's and our own.
    _followees_followers = [auth.user_id]+[row.followees for row in _followees.select(db.follows.followees)]
    #Once we have all of the followers + ourselves, we pull every tweet made by ourselves and those we follow and return them sorted by date.
    tweeters = db(db.tweeters.author.belongs(_followees_followers)).select(orderby=~db.tweeters.date_posted)
    return locals()

def index():
    #Index page that redirects to the feed page
    redirect(URL('default', 'tweets'))

#This is your home page, right now it just shows your profile.
def home():
    user = db.auth_user(request.args(0) or auth.user_id)
    form= SQLFORM(db.auth_user, user, readonly = True,upload = URL('download'))
    #check login
    if not user:
        redirect(URL('default','tweets'))
    tweeters = db(db.tweeters.author==user.id).select(orderby=~db.tweeters.date_posted,limitby=(0,100))
    return locals()

#Delete tweets made by the user
def delete():
    del_tweet = db.tweeters(request.args(0))
    db(db.tweeters.id == del_tweet.id).delete()
    redirect(URL('default','home'))
    
#Handles retweeting older posts into new posts by the user.
def retweet():
    retweet = db.tweeters(request.args(0))
    db.tweeters.insert(origauthor = retweet.author,body = retweet.body, picture = retweet.picture, date_posted = request.now,origdate = retweet.date_posted)
    redirect(URL('default', 'tweets'))
    

#Allows the user to edit his profile to possibly change a picture or rather.
def editprof():
    user = db.auth_user(request.args(0) or auth.user_id)
    form= SQLFORM(db.auth_user, user ,editable = True,upload = URL('download'))
    if form.process().accepted:
        session.flash = T('Updated')
        redirect(URL('default','home'))
    return locals()

#This is supposed to link you to someone elses profile where you could see info or tweets theyve made
def userprof():
    #Determine which user's wall is to be displayed
    prof = db.auth_user(request.args(0) or auth.user_id)
    form = SQLFORM(db.auth_user, prof, readonly = True, upload = URL('download'))
    if not prof:
        redirect(URL('home'))
    twerts = db(db.tweeters.author==prof.id).select(orderby=~db.tweeters.date_posted,limitby=(0,100))
    return locals()

#Allows the user to search for keywords in tweets like # or whatever
def tweetsearch():
    #Create a form for a search field,
    form = SQLFORM.factory(Field('tweetquery', label="Search Tweets", requires=IS_NOT_EMPTY()))
    #Check if the form is accepted.
    if form.accepts(request):
        #Get the form variable from the submission
        tweet = form.vars.tweetquery
        #Lambda is essentially a temp function, and here we use reduce and return a value in search for any iteration of a keyword
        #received by the form submission's body text. This is used several times throughout our program for search returns.
        search = reduce(lambda a,b:a&b,
                         [db.tweeters.body.contains(t) for t in tweet])
        result = db(search).select()
    else:
        result = []
    return locals()

#Function to follow people
@auth.requires_login()
def follow():
    if request.env.request_method!='POST': raise HTTP(400)
    #Here we check if we intend to follow a user
    if request.args(0) =='follow' and not db.follows(followers=auth.user_id, followees=request.args(1)):
        #IF we do, make a record, we are making it so that the user follows said person, also that said person now has a new follower
        #We make a record to both persons, the user and the intended follower, because both need to have records changed.
        db.follows.insert(followers=auth.user_id,followees=request.args(1))
    elif request.args(0)=='unfollow':
        #IF we do not, we are unfollowing, so we need to not only delete a follower, but a followee for said person.
        #Thus we change both records.
        db(db.follows.followers==auth.user_id)(db.follows.followees==request.args(1)).delete()

#Used for grabbing images for user profiles and image posting
@cache.action()
def download():
    """
    allows downloading of uploaded files
    http://..../[app]/default/download/[filename]
    """
    return response.download(request, db)



def user():
    """
    exposes:
    http://..../[app]/default/user/login
    http://..../[app]/default/user/logout
    http://..../[app]/default/user/register
    http://..../[app]/default/user/profile
    http://..../[app]/default/user/retrieve_password
    http://..../[app]/default/user/change_password
    http://..../[app]/default/user/manage_users (requires membership in
    use @auth.requires_login()
        @auth.requires_membership('group name')
        @auth.requires_permission('read','table name',record_id)
    to decorate functions that need access control
    """
    return dict(form=auth())


def call():
    """
    exposes services. for example:
    http://..../[app]/default/call/jsonrpc
    decorate with @services.jsonrpc the functions to expose
    supports xml, json, xmlrpc, jsonrpc, amfrpc, rss, csv
    """
    return service()


@auth.requires_login()
def api():
    """
    this is example of API with access control
    WEB2PY provides Hypermedia API (Collection+JSON) Experimental
    """
    from gluon.contrib.hypermedia import Collection
    rules = {
        '<tablename>': {'GET':{},'POST':{},'PUT':{},'DELETE':{}},
        }
    return Collection(db).process(request,response,rules)
