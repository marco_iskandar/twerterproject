# -*- coding: utf-8 -*-
# this file is released under public domain and you can use without limitations

#########################################################################
## Customize your APP title, subtitle and menus here
#########################################################################

DEVELOPMENT_MENU = False
response.title = "Twerter"
if auth.user:
    response.menu = [

        (T('Home'), False, URL('default', 'home')),
        (T('Feed'), False, URL('default', 'tweets')),
        (T('Following'), False, URL('default', 'following')),
        (T('Find Friends'), False,URL('default', 'search')),
        (T('Search Tweets'), False,URL('default', 'tweetsearch')),
        (T('Edit Profile'), False, URL('default', 'editprof')),
        
    ]
else:
    redirect
