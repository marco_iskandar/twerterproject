# -*- coding: utf-8 -*-
from datetime import datetime

#Handles procuring the name of users
def get_name():
    name = 'Nobody'
    if auth.user:
        name = auth.user.first_name + " " + auth.user.last_name
    return name

#Handles procuring the first name of users.
def get_first_name():
    firstname = 'Nobody'
    if auth.user:
        firstname = auth.user.first_name
    return firstname

#Handles procuring the last name of users.
def get_last_name():
    lastname = 'Nobody'
    if auth.user:
        lastname = auth.user.last_name
        return lastname
    
#Handles procuring the email of users.
def get_email():
    email = 'None'
    if auth.user:
        email = auth.user.email
    return email

#Returns a string of the users full name
def name_of(user): 
    return '%(first_name)s %(last_name)s' % user

#Handles reworking the registration page to add more pieces to it
auth = Auth(db)
auth.settings.extra_fields['auth_user']= [
  Field('city'),
  Field('image','upload')]
auth.define_tables(username=True)
db.auth_user.id.readable =False;

#The table to handle actual twerts
db.define_table('tweeters',
                Field('author', 'reference auth_user'),
                Field('date_posted', 'datetime'),
                Field('body', 'text'),
                Field('origauthor', 'reference auth_user', default = auth.user_id),
                Field('origdate','datetime'),
                Field('picture', type='upload'))

#The table to handle relationships between users.
db.define_table('follows',
                Field('followers', 'reference auth_user'),
                Field('followees', 'reference auth_user')
               )

db.tweeters.body.requires = IS_LENGTH(200, 1)
db.tweeters.date_posted.represent = lambda value, row: value.strftime("%d-%m-%Y %H:%M")
db.tweeters.date_posted.readable=False
db.tweeters.date_posted.writable=False
db.tweeters.author.readable=False
db.tweeters.author.writable=False
db.tweeters.author.default = auth.user_id
db.tweeters.date_posted.default = datetime.utcnow()
db.tweeters.origauthor.readable = False
db.tweeters.origauthor.writable = False
db.tweeters.origdate.readable = False
db.tweeters.origdate.writable = False
